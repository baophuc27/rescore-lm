FROM nvcr.io/nvidia/tritonserver:21.10-py3

COPY ./requirements.txt .


RUN pip install -r requirements.txt

CMD tritonserver --model-repository=/models --log-verbose=0
