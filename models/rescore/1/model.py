from ast import YieldFrom
import logging
import os
import sys
from typing import Dict,Callable,Iterable,List,Optional, Union

import json
import numpy as np
import torch
import triton_python_backend_utils as pb_utils
import functools
import operator

from fairseq.models.transformer_lm import TransformerLanguageModel


logger = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.getLevelName("INFO"),
    handlers=[logging.StreamHandler(sys.stdout)],
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    encoding="utf-8"
)

class RescoreLM:
    def __init__(self,model_path,tokenizer):
        *dir, model_name = model_path.split("/")
        model_dir = "/".join(dir)
        self.model = TransformerLanguageModel.from_pretrained(model_dir,model_name)

    def score(self,texts):
        return self.model.score(texts)

class ScoreRanker:
    def __init__(self) -> None:
        pass

    def predict(self,features) -> int:
        max_idx = np.argmax(np.sum(features,axis=1))
        return max_idx

    
class TritonPythonModel:
    def initialize(self, args : Dict[str,str]):
        path: str = os.path.join(args["model_repository"], args["model_version"])
        model_name: str = args["model_name"]
        logging.info("=======ARGS=========")
        logging.info(args)
        self.rescore_model = RescoreLM("/models/rescore/1/checkpoint_best.pt","space")
        self.rerank_model = ScoreRanker()
        
        logger.info("Rescore model loaded...")
        
    
    def execute(self,requests):
        with torch.inference_mode():
            responses = []
            logger.info(f'Handling a request of batch {len(requests)} requests...')
            
            for request in requests:
                texts = [t[0].decode("utf-8").strip() for t in pb_utils.get_input_tensor_by_name(request, "text").as_numpy().tolist()]
                am_scores = pb_utils.get_input_tensor_by_name(request,"am_score").as_numpy()
                old_scores = pb_utils.get_input_tensor_by_name(request,"nolm_score").as_numpy()
                withlm_scores = pb_utils.get_input_tensor_by_name(request,"withlm_score").as_numpy()
                

                scores = self.rescore_model.score(texts)
                ppl_scores = np.array([[torch.sum(item['positional_scores'].neg())] for item in scores])

                features = np.hstack((am_scores,old_scores,withlm_scores,withlm_scores-old_scores,ppl_scores))
                
                max_idx = self.rerank_model.predict(features)

                output_text = texts[max_idx]
    
                # for item in ppl_scores[:1]: 
                #     ppl_score = torch.sum(item['positional_scores'].neg())

                #     logging.info("{:<40}  {:<30} ".format(
                #         f"Transcript: {texts[0]}",
                #         f"PPL Score: {round(ppl_score.item(),2)}",
                #     ))

                result = pb_utils.Tensor("output_text", np.array(am_scores, dtype=np.object))

                inference_response = pb_utils.InferenceResponse(output_tensors=[result])
                responses.append(inference_response)

        logging.info(len(responses))
        return responses

    def finalize(self):
        logger.info("Cleaning up...")