curl --location --request POST 'http://localhost:6000/v2/models/rescore/infer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "inputs": [
        {
            "name": "text",
            "shape": [
                3, 1
            ],
            "datatype": "BYTES",
            "data": [
                ["mở nhạc sơn tùng","mở nhạc đông nhi","mở nhạc bảo thy"]
            ]
        },
        {
            "name" : "am_score",
            "shape" : [
                3,1
            ],
            "datatype": "FP32",
            "data": [ [1.2,1.3,1.4] ]
        },
        {
            "name" : "nolm_score",
            "shape" : [
                3,1
            ],
            "datatype": "FP32",
            "data": [ [2.2,1.3,1.4] ]
        },
        {
            "name" : "withlm_score",
            "shape" : [
                3,1
            ],
            "datatype": "FP32",
            "data": [ [1.2,1.3,1.4] ]
        }
    ],
    "outputs": [
        {
            "name": "output_text"
        }
    ]
}'